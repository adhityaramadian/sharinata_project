<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-US" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--
    Document Title
    =============================================
    -->
    <title>SHARINATA | SAMARINDA</title>
    <!--
    Favicons
    =============================================
    -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('assets/img/favicons/apple-icon-57x57.png');?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('assets/img/favicons/apple-icon-60x60.png');?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('assets/img/favicons/apple-icon-72x72.png');?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/favicons/apple-icon-76x76.png');?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('assets/img/favicons/apple-icon-114x114.png');?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('assets/img/favicons/apple-icon-120x120.png');?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('assets/img/favicons/apple-icon-144x144.png');?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('assets/img/favicons/apple-icon-152x152.png');?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('assets/img/favicons/apple-icon-180x180.png');?>">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url('assets/img/favicons/android-icon-192x192.png');?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/img/favicons/favicon-32x32.png');?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('assets/img/favicons/favicon-96x96.png');?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/img/favicons/favicon-16x16.png');?>">
    <link rel="manifest" href="<?php echo base_url('assets/img/favicons/manifest.json');?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url('assets/img/favicons/ms-icon-144x144.png');?>">
    <meta name="theme-color" content="#ffffff">
    <!--
    Stylesheets
    =============================================

    -->
    <!-- Default stylesheets-->
    <link href="<?php echo base_url('assets/theme/lib/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Template specific stylesheets-->
    <!--
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    -->
    <link href="<?php echo base_url('assets/theme/font/roboto.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/theme/font/volkhov.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/theme/font/opensans.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/theme/lib/animate.css/animate.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/theme/lib/components-font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/theme/lib/et-line-font/et-line-font.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/theme/lib/flexslider/flexslider.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/theme/lib/owl.carousel/dist/assets/owl.carousel.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/theme/lib/owl.carousel/dist/assets/owl.theme.default.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/theme/lib/magnific-popup/dist/magnific-popup.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/theme/lib/simple-text-rotator/simpletextrotator.css');?>" rel="stylesheet">
    <!-- Main stylesheet and color file-->
    <link href="<?php echo base_url('assets/theme/css/style.css');?>" rel="stylesheet">
    <link id="color-scheme" href="<?php echo base_url('assets/theme/css/colors/default.css');?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/my_style.css');?>">
</head>
<body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
<main>
    <div class="page-loader">
        <div class="loader">Loading...</div>
    </div>
    <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url();?>">
                    <i></i>Sharinata
                </a>
            </div>
            <div class="collapse navbar-collapse" id="custom-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#aboutus">Tentang Kami</a></li>
                    <li><a href="#">Produk</a></li>
                    <li><a href="#">Berita</a></li>
                    <li><a href="#">Promo</a></li>
                    <li><a href="#">A & Q</a></li>
                    <li><a href="#">Keranjang Belanja</a></li>
                    <li><a href="#">Login</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <section class="home-section home-full-height bg-dark-30" id="home" data-background="<?php echo site_url('assets/img/Masterplan_Sharinata.jpg');?>">
        <div class="video-player" data-property="{videoURL:'https://www.youtube.com/watch?v=bNucJgetMjE', containment:'.home-section', startAt:18, mute:false, autoPlay:true, loop:true, opacity:1, showControls:false, showYTLogo:false, vol:25}"></div>
        <div class="video-controls-box">
            <div class="container">
                <div class="video-controls"><a class="fa fa-volume-up" id="video-volume" href="#">&nbsp;</a><a class="fa fa-pause" id="video-play" href="#">&nbsp;</a></div>
            </div>
        </div>
        <div class="titan-caption">
            <div class="caption-content">
                <div class="font-alt mb-10 titan-title-size-1">Halo &amp; Selamat Datang</div>
                <div class="font-alt mb-10 titan-title-size-4">Sharinata</div>
                <div class="font-alt titan-title-size-2">City of Disruption & Sharia</div>
                <a class="section-scroll btn btn-border-w btn-round mt-50" href="#">Pesan Sekarang</a>
            </div>
        </div>
    </section>
    <div class="main">
        <!-- ABOUT US/TENTANG KAMI -->
        <section class="module pt-0" id="about" style="padding-bottom: 1px !important;">
            <div class="row position-relative m-0">
                <div class="col-xs-12 col-md-6 side-image" data-background="<?php echo site_url('assets/img/Masterplan_Sharinata.jpg');?>" style="background-size: contain;"></div>
                <div class="col-xs-12 col-md-6 col-md-offset-6 side-image-text">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2 class="module-title font-alt align-center">Tentang Kami</h2>
                            <div class="module-subtitle font-serif align-center">KONSEP KOTA MANDIRI IDEAL DI JAMAN NOW</div>
                            <img class="img-responsive" src="<?php echo base_url('assets/img/Bismillah.png');?>" style="height: 60px; margin-left: auto; margin-right: auto; display: block;">
                            <p style="margin-top: 6rem;">ISLAM ADALAH AGAMA KEHIDUPAN, yang berarti islam itu ajaran untuk menjadikan kehidupan manusia ini lebih baik. Dengan demikian maka seluruh ajaran islam dapat diimplementasikan dalam kehidupan sehari-hari. Setelah sekian lama masyarakat diberikan pilihan sistem kapitalis dengan perbankan sebagai tolak ukur perekonomian dan masyarakat terkepung dengan riba yang menyulitkan, maka perlu adanya sistem baru yang diperkenalkan kepada masyarakat sebagai suatu alternatif solusi atas banyaknya masalah yang muncul di masyarakat, khususnya dalam masalah kepemilikan perumahan.</p>
                            <p>
                                <a href="http://sharinata.com/tentang_kami" style="color: #19a7d2;"><em>lebih lanjut ...</em></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- LOKASI KAMI -->
        <section class="module pt-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <h2 class="module-title font-alt" style="margin: 70px 0px 20px 0px;">Lokasi Kami</h2>
                        <!--
                        <div class="module-subtitle font-serif">A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</div>
                        -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-6" style="height: 500px">
                        <div id="map"></div>
                    </div>
                    <div class="col-sm-6 col-md-6" style="background-repeat: no-repeat;background-size: cover; background-image: url('<?php echo base_url('assets/img/denah_peta_lokasi.jpg');?>'); height: 500px;"></div>
                </div>
            </div>
        </section>

        <!-- PERSEMBAHAN KAMI -->
        <section class="module pt-0" id="news">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <h2 class="module-title font-alt">Persembahan Kami</h2>
                        <div class="module-subtitle font-serif">Cluster pilihan dengan fasilitas kekinian menambah kenyamanan hunian madani</div>
                    </div>
                </div>
                <div class="row multi-columns-row post-columns">
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="post mb-20">
                            <div class="post-thumbnail"><a href="#"><img src="<?php echo site_url('assets/img/cluster/andalusia_gate.jpg');?>" alt="Cluster Image"/></a></div>
                            <div class="post-header font-alt">
                                <h2 class="post-title"><a href="#">Andalusia</a></h2>
                                <div class="post-meta"></div>
                            </div>
                            <div class="post-entry">
                                <p>cluster profile</p>
                            </div>
                            <div class="post-more"><a class="more-link" href="#">lebih lanjut</a></div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="post mb-20">
                            <div class="post-thumbnail"><a href="#"><img src="<?php echo site_url('assets/img/cluster/constantine_gate.jpg');?>" alt="Cluster Image"/></a></div>
                            <div class="post-header font-alt">
                                <h2 class="post-title"><a href="#">Constantine</a></h2>
                                <div class="post-meta"></div>
                            </div>
                            <div class="post-entry">
                                <p>cluster profile</p>
                            </div>
                            <div class="post-more"><a class="more-link" href="#">lebih lanjut</a></div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="post mb-20">
                            <div class="post-thumbnail"><a href="#"><img src="<?php echo site_url('assets/img/cluster/lubuk_sawah_valley_gate.jpg');?>" alt="Cluster Image"/></a></div>
                            <div class="post-header font-alt">
                                <h2 class="post-title"><a href="#">Lubuk Sawah Valley</a></h2>
                                <div class="post-meta"></div>
                            </div>
                            <div class="post-entry">
                                <p>cluster profile</p>
                            </div>
                            <div class="post-more"><a class="more-link" href="#">lebih lanjut</a></div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="post mb-20">
                            <div class="post-thumbnail"><a href="#"><img src="<?php echo site_url('assets/img/cluster/tursina_gate.jpg');?>" alt="Cluster Image"/></a></div>
                            <div class="post-header font-alt">
                                <h2 class="post-title"><a href="#">Tursina</a></h2>
                                <div class="post-meta"></div>
                            </div>
                            <div class="post-entry">
                                <p>cluster profile</p>
                            </div>
                            <div class="post-more"><a class="more-link" href="#">lebih lanjut</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- PENAWARAN KAMI -->
        <section class="module pt-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <h2 class="module-title font-alt">Penawaran Kami</h2>
                        <div class="module-subtitle font-serif">Tipe dan model hunian untuk kenyamanan dan ketenangan hidup keluarga madani.</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="filter font-alt" id="filters">
                            <li><a class="current wow fadeInUp" href="#" data-filter="*">All</a></li>
                            <li><a class="wow fadeInUp" href="#" data-filter=".andalusia" data-wow-delay="0.2s">Andalusia</a></li>
                            <li><a class="wow fadeInUp" href="#" data-filter=".constantine" data-wow-delay="0.4s">Constantine</a></li>
                            <li><a class="wow fadeInUp" href="#" data-filter=".lubuksawahvalley" data-wow-delay="0.6s">Lubuk Sawah Valley</a></li>
                            <li><a class="wow fadeInUp" href="#" data-filter=".tursina" data-wow-delay="0.6s">Tursina</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--
            illustration -- andalusia
            marketing -- constantine
            photography -- lubuksawahvalley
            webdesign -- tursina
            -->
            <ul class="works-grid works-grid-gut works-hover-w works-grid-4" id="works-grid">
                <li class="work-item andalusia lubuksawahvalley tursina">
                    <a href="<?php echo base_url();?>">
                        <div class="work-image"><img src="<?php echo site_url('assets/img/unit/no-image-available.jpg');?>" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Nama Model</h3>
                            <div class="work-descr">Tipe Hunian <p>Nama Cluster/Sub Cluster</p></div>
                        </div>
                    </a>
                </li>
                <li class="work-item constantine">
                    <a href="<?php echo base_url();?>">
                        <div class="work-image"><img src="<?php echo site_url('assets/img/unit/constantine_type_36-84_unit.jpg');?>" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Sharinata 36</h3>
                            <div class="work-descr">Type 36/84</div>
                        </div>
                    </a>
                </li>
                <li class="work-item constantine">
                    <a href="<?php echo base_url();?>">
                        <div class="work-image"><img src="<?php echo site_url('assets/img/unit/type_45-96_unit.jpg');?>" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Sharinata 45</h3>
                            <div class="work-descr">Type 45/96</div>
                        </div>
                    </a>
                </li>
                <li class="work-item constantine">
                    <a href="<?php echo base_url();?>">
                        <div class="work-image"><img src="<?php echo site_url('assets/img/unit/type_114-120_unit.jpg');?>" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Type 114/120</h3>
                            <div class="work-descr">Tiype 114/120</div>
                        </div>
                    </a>
                </li>
                <li class="work-item constantine">
                    <a href="<?php echo base_url();?>">
                        <div class="work-image"><img src="<?php echo site_url('assets/img/unit/villa_a_90-120_unit.jpg');?>" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Villa A</h3>
                            <div class="work-descr">Tiype 90/120</div>
                        </div>
                    </a>
                </li>
                <li class="work-item constantine">
                    <a href="<?php echo base_url();?>">
                        <div class="work-image"><img src="<?php echo site_url('assets/img/unit/villa_b_90-120_unit.jpg');?>" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Villa B</h3>
                            <div class="work-descr">Tiype 90/120</div>
                        </div>
                    </a>
                </li>
            </ul>
        </section>

        <!-- BERITA/NEWS -->
        <section class="module pt-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <h2 class="module-title font-alt">Berita Sharinata</h2>
                        <div class="module-subtitle font-serif">Informasi ter-<em>update</em> mengenai perkembangan Sharinata City</div>
                    </div>
                </div>
                <div class="row">
                    <div class="owl-carousel text-center" data-items="5" data-pagination="false" data-navigation="false">
                        <div class="owl-item">
                            <div class="col-sm-12">
                                <div class="ex-product"><a href="#"><img src="<?php echo base_url('assets/img/news/no-image.png');?>" alt="News Title"/></a>
                                    <h4 class="shop-item-title font-alt"><a href="#">Headline News</a></h4>sub title
                                </div>
                            </div>
                        </div>
                        <div class="owl-item">
                            <div class="col-sm-12">
                                <div class="ex-product"><a href="#"><img src="<?php echo base_url('assets/img/news/no-image.png');?>" alt="News Title"/></a>
                                    <h4 class="shop-item-title font-alt"><a href="#">Headline News</a></h4>sub title
                                </div>
                            </div>
                        </div>
                        <div class="owl-item">
                            <div class="col-sm-12">
                                <div class="ex-product"><a href="#"><img src="<?php echo base_url('assets/img/news/no-image.png');?>" alt="News Title"/></a>
                                    <h4 class="shop-item-title font-alt"><a href="#">XXXXXXXXXMXXXXX</a></h4>sub title
                                </div>
                            </div>
                        </div>
                        <div class="owl-item">
                            <div class="col-sm-12">
                                <div class="ex-product"><a href="#"><img src="<?php echo base_url('assets/img/news/no-image.png');?>" alt="News Title"/></a>
                                    <h4 class="shop-item-title font-alt"><a href="#">XXXXXXXXXMXXXXX</a></h4>sub title
                                </div>
                            </div>
                        </div>
                        <div class="owl-item">
                            <div class="col-sm-12">
                                <div class="ex-product"><a href="#"><img src="<?php echo base_url('assets/img/news/no-image.png');?>" alt="News Title"/></a>
                                    <h4 class="shop-item-title font-alt"><a href="#">XXXXXXXXXMXXXXXXXXXM</a></h4>sub title
                                </div>
                            </div>
                        </div>
                        <div class="owl-item">
                            <div class="col-sm-12">
                                <div class="ex-product"><a href="#"><img src="<?php echo base_url('assets/img/news/no-image.png');?>" alt="News Title"/></a>
                                    <h4 class="shop-item-title font-alt"><a href="#">max. 20 angka huruf tanda baca</a></h4>sub title
                                </div>
                            </div>
                        </div>
                        <div class="owl-item">
                            <div class="col-sm-12">
                                <div class="ex-product"><a href="#"><img src="<?php echo base_url('assets/img/news/no-image.png');?>" alt="News Title"/></a>
                                    <h4 class="shop-item-title font-alt"><a href="#">Headline News</a></h4>sub title
                                </div>
                            </div>
                        </div>
                        <div class="owl-item">
                            <div class="col-sm-12">
                                <div class="ex-product"><a href="#"><img src="<?php echo base_url('assets/img/news/no-image.png');?>" alt="News Title"/></a>
                                    <h4 class="shop-item-title font-alt"><a href="#">Headline News</a></h4>sub title
                                </div>
                            </div>
                        </div>
                        <div class="owl-item">
                            <div class="col-sm-12">
                                <div class="ex-product"><a href="#"><img src="<?php echo base_url('assets/img/news/no-image.png');?>" alt="News Title"/></a>
                                    <h4 class="shop-item-title font-alt"><a href="#">Headline News</a></h4>sub title
                                </div>
                            </div>
                        </div>
                        <div class="owl-item">
                            <div class="col-sm-12">
                                <div class="ex-product"><a href="#"><img src="<?php echo base_url('assets/img/news/no-image.png');?>" alt="News Title"/></a>
                                    <h4 class="shop-item-title font-alt"><a href="#">Headline News</a></h4>sub title
                                </div>
                            </div>
                        </div>
                        <div class="owl-item">
                            <div class="col-sm-12">
                                <div class="ex-product"><a href="#"><img src="<?php echo base_url('assets/img/news/no-image.png');?>" alt="News Title"/></a>
                                    <h4 class="shop-item-title font-alt"><a href="#">Headline News</a></h4>sub title
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- PROMO -->
        <section class="module pt-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <h2 class="module-title font-alt">Promo</h2>
                        <div class="module-subtitle font-serif">Dapatkan hunian impian dengan harga khusus</div>
                    </div>
                </div>
                <div class="row">
                    <div class="">
                        <div class="col-sm-3 menu">
                            <div class="col-sm-12">
                                <h4 class="menu-title font-alt">TITLE PROMOSI</h4>
                                <div class="menu-detail font-serif">SUB TITLE PROMOSI</div>
                                <table class="table">
                                    <tr>
                                        <th>Harga Unit</th>
                                        <td class="text-right">Rp. 999.999.999</td>
                                    </tr>
                                    <tr>
                                        <th>Harga Promo</th>
                                        <td class="text-right">Rp. 999.999.999</td>
                                    </tr>
                                    <tr>
                                        <th>Harga <em>n</em> tahun</th>
                                        <td class="text-right">Rp. 999.999.999</td>
                                    </tr>
                                    <tr>
                                        <th>Uang Muka</th>
                                        <td class="text-right">Rp. 999.999.999</td>
                                    </tr>
                                    <tr>
                                        <th>Serah Terima</th>
                                        <td class="text-right">999 bulan</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Angsuran Ke</th>
                                                    <th class="text-center">Angsuran</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>xx-xx</td>
                                                    <td class="text-right" style="padding: 0 6px;">Rp. 999.999.999</td>
                                                </tr>
                                                <tr>
                                                    <td>xx-xx</td>
                                                    <td class="text-right" style="padding: 0 6px;">Rp. 999.999.999</td>
                                                </tr>
                                                <tr>
                                                    <td>xx-xx</td>
                                                    <td class="text-right" style="padding: 0 6px;">Rp. 999.999.999</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--
                            <div class="col-sm-4 p-0" style="border: 1px solid #ff0000; height: 293px;">
                                <h4 class="inner rotate_text">Rp. 999.999.999</h4>
                            </div>
                            -->
                            <div class="col-sm-12 text-center">
                                <button class="btn btn-border-d btn-circle" type="submit">Pesan</button>
                            </div>
                        </div>

                        <div class="col-sm-3 menu">
                            <div class="col-sm-12">
                                <h4 class="menu-title font-alt">TITLE PROMOSI</h4>
                                <div class="menu-detail font-serif">SUB TITLE PROMOSI</div>
                                <table class="table">
                                    <tr>
                                        <th>Harga Unit</th>
                                        <td class="text-right" style="color: #dddddd;">Rp. <strike>999.999.999</strike></td>
                                    </tr>
                                    <tr>
                                        <th>Harga Promo</th>
                                        <td class="text-right">Rp. 999.999.999</td>
                                    </tr>
                                    <tr>
                                        <th>Harga <em>n</em> tahun</th>
                                        <td class="text-right">Rp. 999.999.999</td>
                                    </tr>
                                    <tr>
                                        <th>Uang Muka</th>
                                        <td class="text-right">Rp. 999.999.999</td>
                                    </tr>
                                    <tr>
                                        <th>Serah Terima</th>
                                        <td class="text-right">999 bulan</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Angsuran Ke</th>
                                                    <th class="text-center">Angsuran</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>xx-xx</td>
                                                    <td class="text-right" style="padding: 0 6px;">Rp. 999.999.999</td>
                                                </tr>
                                                <tr>
                                                    <td>xx-xx</td>
                                                    <td class="text-right" style="padding: 0 6px;">Rp. 999.999.999</td>
                                                </tr>
                                                <tr>
                                                    <td>xx-xx</td>
                                                    <td class="text-right" style="padding: 0 6px;">Rp. 999.999.999</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--
                            <div class="col-sm-4 menu-price-detail p-0">
                                <h4 class="menu-price font-alt">Rp. <strike style="color: #dddddd;">999.999.999</strike></h4>
                            </div>
                            -->
                            <div class="col-sm-12 text-center">
                                <button class="btn btn-border-d btn-circle" type="submit">Pesan</button>
                            </div>
                        </div>

                        <div class="col-sm-3 menu">
                            <div class="col-sm-12">
                                <h4 class="menu-title font-alt">TITLE PROMOSI</h4>
                                <div class="menu-detail font-serif">SUB TITLE PROMOSI</div>
                                <table class="table">
                                    <tr>
                                        <th>Harga Unit</th>
                                        <td class="text-right">Rp. 999.999.999</td>
                                    </tr>
                                    <tr>
                                        <th>Harga Promo</th>
                                        <td class="text-right"></td>
                                    </tr>
                                    <tr>
                                        <th>Harga <em>n</em> tahun</th>
                                        <td class="text-right">Rp. 999.999.999</td>
                                    </tr>
                                    <tr>
                                        <th>Uang Muka</th>
                                        <td class="text-right">Rp. 999.999.999</td>
                                    </tr>
                                    <tr>
                                        <th>Serah Terima</th>
                                        <td class="text-right">999 bulan</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Angsuran Ke</th>
                                                    <th class="text-center">Angsuran</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>xx-xx</td>
                                                    <td class="text-right" style="padding: 0 6px;">Rp. 999.999.999</td>
                                                </tr>
                                                <tr>
                                                    <td>xx-xx</td>
                                                    <td class="text-right" style="padding: 0 6px;">Rp. 999.999.999</td>
                                                </tr>
                                                <tr>
                                                    <td>xx-xx</td>
                                                    <td class="text-right" style="padding: 0 6px;">Rp. 999.999.999</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--
                            <div class="col-sm-4 menu-price-detail p-0">
                                <h4 class="menu-price font-alt">Harga Rp. 999.999.999</h4>
                            </div>
                            -->
                            <div class="col-sm-12 text-center">
                                <button class="btn btn-border-d btn-circle" type="submit">Pesan</button>
                            </div>
                        </div>

                        <div class="col-sm-3 menu">
                            <div class="col-sm-12">
                                <h4 class="menu-title font-alt">TITLE PROMOSI</h4>
                                <div class="menu-detail font-serif">SUB TITLE PROMOSI</div>
                                <table class="table">
                                    <tr>
                                        <th>Harga Unit</th>
                                        <td class="text-right">Rp. 999.999.999</td>
                                    </tr>
                                    <tr>
                                        <th>Harga Promo</th>
                                        <td class="text-right"></td>
                                    </tr>
                                    <tr>
                                        <th>Harga <em>n</em> tahun</th>
                                        <td class="text-right">Rp. 999.999.999</td>
                                    </tr>
                                    <tr>
                                        <th>Uang Muka</th>
                                        <td class="text-right">Rp. 999.999.999</td>
                                    </tr>
                                    <tr>
                                        <th>Serah Terima</th>
                                        <td class="text-right">999 bulan</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Angsuran Ke</th>
                                                    <th class="text-center">Angsuran</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>xx-xx</td>
                                                    <td class="text-right" style="padding: 0 6px;">Rp. 999.999.999</td>
                                                </tr>
                                                <tr>
                                                    <td>xx-xx</td>
                                                    <td class="text-right" style="padding: 0 6px;">Rp. 999.999.999</td>
                                                </tr>
                                                <tr>
                                                    <td>xx-xx</td>
                                                    <td class="text-right" style="padding: 0 6px;">Rp. 999.999.999</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--
                            <div class="col-sm-4 menu-price-detail p-0">
                                <h4 class="menu-price font-alt">Harga Rp. 999.999.999</h4>
                            </div>
                            -->
                            <div class="col-sm-12 text-center">
                                <button class="btn btn-border-d btn-circle" type="submit">Pesan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- MARKETING LIST -->
        <section class="module-small pt-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <h2 class="module-title font-alt">Silakan menghubungi team marketing kami</h2>
                        <div class="module-subtitle font-serif">Kami siap membantu anda miliki unit impian bagi keluarga tercinta.</div>
                    </div>
                </div>
                <div class="row multi-columns-row">
                    <div class="col-sm-6 col-md-3 col-lg-3">
                        <div class="shop-item">
                            <div class="shop-item-image">
                                <img src="<?php echo site_url('assets/img/mkt/no-image-available.png');?>" alt="Marketing Image"/>
                                <div class="shop-item-detail" style="color: #000;">
                                    <i class="fa fa-user"></i> Nama Marketing
                                    <br>
                                    <i class="fa fa-whatsapp"></i> Nomor Handphone
                                    <br>
                                    <i class="fa fa-envelope text-white"></i> Email
                                </div>
                            </div>
                            <h4 class="shop-item-title font-alt"><a href="#">Nama Panggilan</a></h4>
                        </div>
                    </div>
                </div>
                <!--
                <div class="row">
                <div class="col-sm-12">
                <div class="pagination font-alt"><a href="#"><i class="fa fa-angle-left"></i></a><a class="active" href="#">1</a><a href="#">2</a><a href="#">3</a><a href="#">4</a><a href="#"><i class="fa fa-angle-right"></i></a></div>
                </div>
                </div>
                -->
            </div>
        </section>
    </div>

    <footer class="footer bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <p class="copyright font-alt">&copy; 2018&nbsp;<a href="<?php echo base_url();?>">sharinata by indo tata graha</a>, All Rights Reserved</p>
                </div>
                <div class="col-sm-6">
                    <div class="footer-social-links">
                        <!--
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-dribbble"></i></a>
                        <a href="#"><i class="fa fa-skype"></i></a>
                        -->
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
</main>
<!--
JavaScripts
=============================================
-->
<script src="<?php echo base_url('assets/theme/lib/jquery/dist/jquery.js');?>"></script>
<script src="<?php echo base_url('assets/theme/lib/bootstrap/dist/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/theme/lib/wow/dist/wow.js');?>"></script>
<script src="<?php echo base_url('assets/theme/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js');?>"></script>
<script src="<?php echo base_url('assets/theme/lib/isotope/dist/isotope.pkgd.js');?>"></script>
<script src="<?php echo base_url('assets/theme/lib/imagesloaded/imagesloaded.pkgd.js');?>"></script>
<script src="<?php echo base_url('assets/theme/lib/flexslider/jquery.flexslider.js');?>"></script>
<script src="<?php echo base_url('assets/theme/lib/owl.carousel/dist/owl.carousel.min.js');?>"></script>
<script src="<?php echo base_url('assets/theme/lib/smoothscroll.js');?>"></script>
<script src="<?php echo base_url('assets/theme/lib/magnific-popup/dist/jquery.magnific-popup.js');?>"></script>
<script src="<?php echo base_url('assets/theme/lib/simple-text-rotator/jquery.simple-text-rotator.min.js');?>"></script>
<script src="<?php echo base_url('assets/theme/js/plugins.js');?>"></script>
<script src="<?php echo base_url('assets/theme/js/main.js');?>"></script>

<script>
    function initMap() {
        var uluru = {lat: -0.49422, lng: 117.2369};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: uluru,
            mapTypeId: 'hybrid'
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }

    $( document ).ready( function() {
    });
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0Dx_boXQiwvdz8sJHoYeZNVTdoWONYkU&callback=initMap"></script>
</body>
</html>